using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using System.Text;
using Photon.Pun.UtilityScripts;
using Photon.Realtime;
using System.Linq;
using TMPro;
using UnityEngine.SceneManagement;

public class FinalScoreboard : MonoBehaviour
{
    [SerializeField] private TMP_Text scores;
    [SerializeField] private GameObject[] spawnPoints;
    [SerializeField] private GameObject[] skins;
    public List<PhotonPlayer> sortedplayerList;


    private void Start()
    {
        PhotonNetwork.AutomaticallySyncScene = true;
        UpdateScoreboard();
        SpawnOnRanking();

    }


    public void UpdateScoreboard()
    {

        var names = new StringBuilder();

        sortedplayerList = PhotonNetwork.playerList.ToList();
        sortedplayerList.Sort(sortByScore);

        //Get player names
        foreach (PhotonPlayer p in sortedplayerList)
        {
            names.Append(p.NickName + "  Kills: " + p.GetScore() + "\n");
        }

        string output = names.ToString();

        scores.text = output;
    }

    public static int sortByScore(PhotonPlayer a, PhotonPlayer b)
    {
        return b.GetScore().CompareTo(a.GetScore());
    }

    public void BackToLobby()
    {

        /*foreach (var p in PhotonNetwork.playerList)
        {
            p.SetScore(0);
        }*/

        if (PhotonNetwork.player.IsLocal)
        {
            PhotonNetwork.player.SetScore(0);
        }

        PhotonNetwork.LeaveRoom();
        SceneManager.LoadScene("Lobby");


    }

    public void SpawnOnRanking()
    {

        for (int i = 0; i < 3 && i < sortedplayerList.Count; i++)
        {
            int index = (int)sortedplayerList[i].CustomProperties["skin"];
            GameObject player = Instantiate(skins[index], spawnPoints[i].transform.position, Quaternion.identity, spawnPoints[i].transform);
            player.GetComponent<Animator>().Play("Celebrate", 0);
        }

    }

    public void BackToGame()
    {
        if (PhotonNetwork.IsMasterClient)
        {
            foreach (var p in PhotonNetwork.playerList)
            {
                p.SetScore(0);
            }
            //SceneManager.LoadScene("WaitingLobby");
            PhotonNetwork.LoadLevel("WaitingLobby");
        }
        else
        {
            Debug.Log("No Master");
        }
    }
}
