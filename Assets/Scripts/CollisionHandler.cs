using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.VFX;
using Photon.Pun;
using Photon.Pun.UtilityScripts;
using Photon.Realtime;
using System.Linq;

public class CollisionHandler : MonoBehaviourPun
{
    private Collider sphereCol;
    private PhotonView view;
    private GameObject item;
    public int normalDamage = CharacterProperties.normalDamage;
    public int poisonDamage = CharacterProperties.mushroomDamage;
    public int boostedDamage = CharacterProperties.boostedDamage;
    public int shieldDamage = CharacterProperties.shieldDamage;

    [Space]
    [Header("VFX")]
    [SerializeField] private VisualEffect itemVFX;

    [SerializeField] private Texture2D[] vfxTextures;

    private List<Light> lights;
    private GameObject minimapCamera;
    private bool itemToPlayer = false;
    private int lastDice;
    //[SerializeField] private Material hitMaterial = null;

    private SoundController soundController;
    public AudioSource hitSound;
    private void Awake()
    {

    }

    // Start is called before the first frame update
    void Start()
    {
        hitSound = GetComponent<AudioSource>();
        soundController = FindObjectOfType<SoundController>();
        PlayerController playerController = GetComponent<PlayerController>();
        view = GetComponent<PhotonView>();
        sphereCol = GetComponent<Collider>();



        if (view.IsMine)
        {

            minimapCamera = GameObject.Find("Map Camera").gameObject;
            minimapCamera.GetComponent<Camera>().enabled = false;

        }


    }

    // Update is called once per frame
    void Update()
    {
        DeleteItemAfterAnimation();
        if (photonView.IsMine)
        {
            if (itemToPlayer)
            {
                ItemOverPlayer();
            }
        }




    }

    void OnCollisionEnter(Collision collision)
    {

        if (collision.gameObject.CompareTag("Autsch") && !collision.gameObject.GetComponentInParent<PhotonView>().IsMine)
        {

            PlayerController enemy = collision.transform.root.gameObject.GetComponent<PlayerController>();

            if (view.IsMine)
            {

                view.RPC("PlayAtPoint", RpcTarget.All, "HitSoundBlunt", collision.contacts[0].point);

                int skin = (int) PhotonNetwork.player.CustomProperties["skin"];

                view.RPC("PlayAtPoint", RpcTarget.All, "HurtSound"+skin, collision.contacts[0].point);

                if (enemy.morePower)
                {
                    gameObject.GetComponent<PlayerController>().TakeDamage(boostedDamage);
                }
                else if (gameObject.GetComponent<PlayerController>().isShielded)
                {
                    if (view.IsMine)
                    {
                        gameObject.GetComponent<PlayerController>().TakeDamage(shieldDamage);
                    }

                }
                else
                {
                    if (view.IsMine)
                    {
                        gameObject.GetComponent<PlayerController>().TakeDamage(normalDamage);
                    }

                }


                if (gameObject.GetComponent<PlayerController>().isDead)
                {

                    enemy.view.Owner.AddScore(1);
                    if (item != null)
                    {
                        PhotonNetwork.Destroy(item);
                    }
                }

            }

        }

    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Item" && !itemToPlayer)

        {
            if (view.IsMine)
            {
                item = other.gameObject;
                PhotonNetwork.Destroy(item);
                soundController.Play("DiceSound");
                item = PhotonNetwork.Instantiate("Dice_d6_dummy", 
                    transform.position + new Vector3(0, 5, -0.5f),
                    Quaternion.identity);

                itemToPlayer = true;

                choice();
            }
        }
    }


    [PunRPC]
    public void PlayAtPoint(string name, Vector3 point)
    {
        AudioSource.PlayClipAtPoint(soundController.GetAudioClip(name), point);
    }


    public void choice()
    {

        int dice = Random.Range(1, 7);
        Animator itemAnimator = item.GetComponent<Animator>();

        switch (dice)
        {
            case 1:
                Debug.Log("Finsternis f�r Alle");
                itemAnimator.Play("Curse", 0);

                break;
            case 2:
                Debug.Log("Schild");
                itemAnimator.Play("Shield", 0);
                break;
            case 3:
                Debug.Log("Mehr Schaden");
                itemAnimator.Play("Damage", 0);

                break;
            case 4:
                Debug.Log("Vergiftung");
                itemAnimator.Play("Mushroom", 0);

                break;
            case 5:
                Debug.Log("Minimap");
                itemAnimator.Play("Minimap", 0);

                break;
            case 6:
                Debug.Log("Niete");
                itemAnimator.Play("Niete", 0);

                break;

            default:
                break;

        }

        lastDice = dice;

    }

    
    void DeleteItemAfterAnimation()
    {

        if (item != null && item.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsTag("End") && item.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).normalizedTime >= 1)
        {
            if (photonView.IsMine)
            {
                switch (lastDice)
                {
                    case 1:
                        soundController.Play("CurseSound");
                        DarknessforAll();
                        break;

                    case 2:
                        soundController.Play("ShieldSound");
                        itemVFX.SetTexture("MainTex", vfxTextures[0]);
                        itemVFX.enabled = true;
                        Shield();
                        break;

                    case 3:
                        soundController.Play("DamageBoostSound");
                        itemVFX.SetTexture("MainTex", vfxTextures[1]);
                        itemVFX.enabled = true;
                        MoreDamage();
                        break;

                    case 4:
                        soundController.Play("PoisonSound");
                        itemVFX.SetTexture("MainTex", vfxTextures[2]);
                        itemVFX.enabled = true;
                        Poison();
                        break;
                    case 5:
                        soundController.Play("MinimapSound");
                        Minimap();
                        break;

                    case 6:
                        soundController.Play("NieteSound");
                        break;

                    default:
                        break;
                }
            }

            if (view.IsMine)
            {
                PhotonNetwork.Destroy(item);
                itemToPlayer = false;
                item = null;
            }


        }
    }




    void ItemOverPlayer()
    {
        if (view.IsMine)
        {
            if (itemToPlayer && item != null)
            {
                item.transform.position = this.transform.position + new Vector3(0, 5, -0.5f);
            }
        }

    }

    void DarknessforAll()
    {
        StartCoroutine(DarknessCoroutine(10));
    }
    void Shield()
    {
        StartCoroutine(ShieldDamageCoroutine(10, false));
    }
    void Poison()
    {
        StartCoroutine(PoisonCoroutine(2));
    }
    void MoreDamage()
    {
        StartCoroutine(ShieldDamageCoroutine(10, true));
    }
    void Minimap()
    {
        StartCoroutine(MinimapCoroutine(30));
    }



    IEnumerator PoisonCoroutine(float i)
    {
        for (int j = 0; j < 8; j++)
        {
            if (view.IsMine)
            {
                gameObject.GetComponent<PlayerController>().TakeDamage(poisonDamage);


                yield return new WaitForSeconds(i);
            }


        }
        itemVFX.enabled = false;

    }

    IEnumerator MinimapCoroutine(float i)
    {

        if (view.IsMine)
        {
            minimapCamera.GetComponent<Camera>().enabled = true;
        }
        yield return new WaitForSeconds(i);

        if (view.IsMine)
        {
            minimapCamera.GetComponent<Camera>().enabled = false;

        }
    }

    IEnumerator DarknessCoroutine(float i)
    {

        lights = FindObjectsOfType<Light>().ToList();
        float[] intensity = new float[lights.Count];

        for (int j = 0; j < lights.Count; j++)
        {
            intensity[j] = lights[j].intensity;
            lights[j].intensity = 0;
        }
        yield return new WaitForSeconds(i);

        for (int j = 0; j < lights.Count; j++)
        {
            lights[j].intensity = intensity[j];
        }


    }
    IEnumerator ShieldDamageCoroutine(float i, bool damage)
    {
        if (damage)
        {
            gameObject.GetComponent<PlayerController>().morePower = true;
            yield return new WaitForSeconds(i);
            gameObject.GetComponent<PlayerController>().morePower = false;
            itemVFX.enabled = false;
        }
        else
        {
            gameObject.GetComponent<PlayerController>().isShielded = true;
            yield return new WaitForSeconds(i);
            gameObject.GetComponent<PlayerController>().isShielded = false;
            itemVFX.enabled = false;
        }


    }


}
