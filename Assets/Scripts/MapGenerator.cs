using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Photon.Pun;
using Photon.Pun.UtilityScripts;

public class MapGenerator : MonoBehaviourPun
{
    [SerializeField] private List<GameObject> mapTiles;
    [SerializeField] private Vector3[] positions;
    private int[] angles = { 0, 90, -90, 180 };
    private int players = 0;
    public static bool playersLoaded = false;
    [SerializeField] private GameObject waitingCanvas;
    private SoundController soundController;
    private void Start()
    {
        PhotonNetwork.AutomaticallySyncScene = true;
        soundController = FindObjectOfType<SoundController>();
    }

    // Von jedem Player an den Master bei OnLvlWasLoaded ne bool schicken, wenn von jedem anderen spieler erhalten, createt der Master die Map
    private void OnLevelWasLoaded(int level)
    {
        if (level == 2)
        {
            waitingCanvas.SetActive(true);
            if (PhotonNetwork.IsMasterClient)
            {
             
            } else
            {
                photonView.RPC("PlayersWhoLoaded",RpcTarget.MasterClient, 1);
            }

        }
        
    }

    [PunRPC]
    public void PlayersWhoLoaded(int i)
    {
        if (PhotonNetwork.IsMasterClient)
        {
            players += i;
            if (players == PhotonNetwork.playerList.Length - 1)
            {
                
                
                StartCoroutine(CreatMapAfterSeconds(5f));
                foreach(var p in PhotonNetwork.playerList)
                {
                    p.SetScore(0);
                }

            }
        }
    }
    [PunRPC]
    public void PlayersLoaded()
    {
        playersLoaded = true;
        waitingCanvas.SetActive(false);
        soundController.Play("GameMusic");
    }
    public void CreateMap()
    {
        mapTiles = mapTiles.OrderBy(x => Random.value).ToList();

        for (int i = 0; i < 4; i++)
        {
            mapTiles[i].transform.Rotate(Vector3.up, angles[(int)Random.Range(0, 4)]);
            mapTiles[i].transform.position = positions[i];
        }

        mapTiles[mapTiles.Count-1].SetActive(false);
        mapTiles[mapTiles.Count-2].SetActive(false);

        if (PhotonNetwork.IsMasterClient)
        {
            gameObject.GetPhotonView().RPC("DisableMapTile", RpcTarget.Others, mapTiles[mapTiles.Count - 1].name);
            gameObject.GetPhotonView().RPC("DisableMapTile", RpcTarget.Others, mapTiles[mapTiles.Count - 2].name);
        }
       

    }

    IEnumerator CreatMapAfterSeconds(float seconds)
    {
        yield return new WaitForSeconds(seconds);
        PlayersLoaded();
        gameObject.GetPhotonView().RPC("PlayersLoaded", RpcTarget.Others);
        CreateMap();
        GameLength.timeStarted = true;

    }

    [PunRPC]
    public void DisableMapTile(string mapTileName)
    {
        foreach (var mTile in mapTiles)
        {
            if (mTile.name.Equals(mapTileName))
            {
                mTile.SetActive(false);
                break;
            }
        }
    }





}
