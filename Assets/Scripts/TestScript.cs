using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestScript : MonoBehaviour
{
    [Space]
    private float moveSpeed = CharacterProperties.moveSpeed;
    private float rotationSpeed = CharacterProperties.rotationSpeed;
    private int angle = CharacterProperties.angle;
    private float sphereForce = CharacterProperties.sphereForce;

    [Space]
    public HealthBar healthBar;
    [SerializeField] private GameObject sphere;
    [SerializeField] private GameObject dust;


    private GameObject minimapCamera;
    private Plane plane = new Plane(Vector3.up, 0);
    [SerializeField] private Rigidbody sphereRig;
    [SerializeField] private Rigidbody playerRig;
    public bool followMouse = true;
    private Vector3 rayDistance;

    // Start is called before the first frame update
    void Start()
    {
        CameraFollow cameraFollow = gameObject.GetComponent<CameraFollow>();
        if (cameraFollow != null)
        {
            
                cameraFollow.OnStartFollowing();
            

        }

    }

    // Update is called once per frame
    void FixedUpdate()
    {
        float _hit;

        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        if (plane.Raycast(ray, out _hit) && followMouse)
        {


            Debug.DrawLine(transform.position, ray.GetPoint(_hit), Color.cyan);

            rayDistance = ray.GetPoint(_hit) - transform.position;

            if (rayDistance.magnitude > 1)
            {
                GetComponent<Animator>().ResetTrigger("isIdle");
                if (!Input.GetMouseButton(1))
                {
                    GetComponent<Animator>().Play("0_Walk", 0);
                }
                playerRig.AddForce((ray.GetPoint(_hit) - transform.position).normalized * moveSpeed, ForceMode.Impulse);
            }
            else
            {
                if (!Input.GetMouseButton(1))
                {

                    GetComponent<Animator>().SetTrigger("isIdle");

                }
            }



            if (Input.GetMouseButton(1))
            {

                
                    transform.Rotate(Vector3.up, -angle * Time.fixedDeltaTime * rotationSpeed);
                    sphereRig.AddForce(transform.right * sphereForce);



            }

            Vector3 mouseDirection = new Vector3(ray.GetPoint(_hit).x, transform.position.y, ray.GetPoint(_hit).z) - transform.position;

            transform.forward = Vector3.Lerp(transform.forward, mouseDirection.normalized * 5, Time.deltaTime * 1.5f);


        }
    }
}
