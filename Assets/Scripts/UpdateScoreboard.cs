using Photon.Pun;
using Photon.Pun.UtilityScripts;
using Photon.Realtime;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TMPro;
using UnityEngine;

public class UpdateScoreboard : MonoBehaviour
{
    int playersOnStart;

    private void Awake()
    {
        playersOnStart = PhotonNetwork.playerList.Length;
    }

    private void Update()
    {
        if(PhotonNetwork.playerList.Length < playersOnStart)
        {
            PhotonNetwork.SetMasterClient(PhotonNetwork.playerList[0]);
            GameLength.timeStarted = true;
            playersOnStart--;
        }
    }

    [SerializeField] TMP_Text scoreboard;
    public string SortScoreList()
    {
        int playercount = PhotonNetwork.playerList.Length;

        var names = new StringBuilder();

        List<PhotonPlayer> sortedplayerList = PhotonNetwork.playerList.ToList();
        sortedplayerList.Sort(sortByScore);
        
        //Get player names
        foreach (PhotonPlayer p in sortedplayerList)
        {
            names.Append(p.NickName + "  Kills: " + p.GetScore() + "\n");
        }

        string output = "Fighters Online:  " + playercount.ToString() + "\n" + names.ToString();
        return output;
    }

   
    public void SetText()
    {
        scoreboard.text = SortScoreList();
    }
    public static int sortByScore(PhotonPlayer a, PhotonPlayer b)
    {
        return b.GetScore().CompareTo(a.GetScore());
    }


}
