using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mover : MonoBehaviour
{
    [SerializeField] float moveSpeed;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        float horizontalInput = Input.GetAxis("Horizontal");
        float verticalInput = Input.GetAxis("Vertical");

        Vector3 movementDirection = new Vector3(horizontalInput, 0, verticalInput);
        movementDirection.Normalize();

        if(!Physics.Raycast(transform.position, movementDirection, 1))
        {
            transform.Translate(movementDirection * moveSpeed * Time.deltaTime, Space.World);

            if (movementDirection != Vector3.zero)
            {
                transform.forward = movementDirection;
            }
        } 
        
    }
}
